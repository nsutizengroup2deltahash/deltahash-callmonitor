#ifndef __callLogMonitor_H__
#define __callLogMonitor_H__

#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <efl_extension.h>
#include <dlog.h>
#include <telephony.h>
#include <system_info.h>
#include <contacts.h>
#include <glib.h>
#include "vector.h"
#include "calculatePackageTotal.h"

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "CLM"

#if !defined(PACKAGE)
#define PACKAGE "com.nsu-samsung.calllogmonitor"
#endif


#define MAX_LEN 500

typedef struct appdata {
	Evas_Object* win;
	Evas_Object* conform;
	Evas_Object* label;
} appdata_s;

typedef struct telephony_data
{
	unsigned int count;
	char* iccid[2];
	char* operator_id[2];
	char* msin[2];
	char* spn[2];
	char* subscriber_number[2];
	char* subscriber_id[2];
	char* mnc[2];
	char* imei[2];
	int cell_id[2];

} my_telephony_data;

/* dialled_call_history */
typedef struct {
	char* call_number;
	int call_duration;
	int sim_id;
	float cost;
} call_information;

typedef struct {
	int log_type;
	int log_time;
	char* call_number;
	int call_duration;
	int sim_slot_no;
} call_record;

typedef struct {
  call_record *array;
  size_t used;
  size_t size;
} Array;

typedef struct {
    int id;
    int mnc;
    char* package_name;
    float cost_per_second;
} QueryData;

typedef struct {
  QueryData *array;
  size_t used;
  size_t size;
} PackageArray;

my_telephony_data my_sim_info;
Array call_record_array;
PackageArray package_array;

void _init_my_telephony_info();
void _print_my_telephony_info();
void _populate_my_history_data(char* call_number, int call_duration, int sim_id);

void print_call_information();
void initArray(Array *a, size_t initialSize);
void insertArray(Array *a, call_record element);
void freeArray(Array *a);
void call_information_init();
void call_information_deinit(void *data);

//void insertQuery(int mnc, char *package_name, float cost_per_second);
//float getCostPerSecondFromPackageName(char* package_name);
//void showQuery();
void calculatePackageTotal(char* operatorId, int callDuration, char* spn);

void populatePackageData();
float getCostPerSecondFromPackageData(char* package_name);

char* getOperatorId(char* num);
void calculateTotal();

vector call_log_information;
vector package_data;

char* selected_package_sim1;
char* selected_package_sim2;

Evas_Object* label1;
Evas_Object* label2;

#endif /* __calllogmonitor_H__ */
