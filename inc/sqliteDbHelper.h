//#include <dlog.h>
//#include <string.h>
//#include <sqlite3.h>
//#include <stdint.h>
//#include <stdlib.h>
//#include <storage.h>
//#include <app_common.h>
//#include <stdio.h>
//
//#define DB_NAME "sample.db"
//#define TABLE_NAME "SampleTable"
//#define COL_ID "ID"
//#define COL_MNC "MNC"
//#define COL_PACKAGE_NAME "PACKAGE_NAME"
//#define COL_COST_PER_SEC "COST_PER_SECOND"
//#define COL_DATE "QR_DATE"
//#define MAX_LEN 30
//
//typedef struct
//{
//    int id;
//    int mnc;
//    char package_name[MAX_LEN];
//    float cost_per_second;
//    char date[MAX_LEN];
//} QueryData;
//
///*insert type, msg in the database. Date will be stored from system and id is autoincrement*/
////int insertMsgIntoDb(int type, const char * msg_data);
//int insertIntoDb(int mnc, const char * package_name, float cost_per_second);
//
///*fetch all stored message form database. This API will return total number of rows found in this call*/
//int getAllFromDb(QueryData **query_data, int* num_of_rows);
//
///*fetch stored message form database based on given ID. Application needs to send desired ID*/
//int getQueryById(QueryData **query_data, int id);
//int getQueryByPackageName(QueryData **query_data, char *package_name);
//
///*delete stored message form database based on given ID. Application needs to send desired ID*/
//int deleteQueryById(int id);
//
///*fetch all stored message form database*/
//int deleteAll();
//
///*count number of stored msg in the database and will return the total number*/
//int getTotalItemsCount(int* num_of_rows);
//
//void database_init();
