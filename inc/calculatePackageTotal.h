#ifndef CALCULATEPACKAGETOTAL_H_
#define CALCULATEPACKAGETOTAL_H_

void calculateTotalDuration(char* spn);
void calculateTotalCostForGrameenPackage();
void calculateTotalCostForAirtelPackage();
void calculateTotalCostForBanglalinkPackage();
void calculateTotalCostForRobiPackage();
void calculateCostBasedonSpn(char* spn);
char* getPackageWithMinimumCost();

int total_duration_same;
int total_duration_other;

float total_cost_package1;
float total_cost_package2;
float total_cost_package3;

char* package1;
char* package2;
char* package3;

#endif /* CALCULATEPACKAGETOTAL_H_ */
