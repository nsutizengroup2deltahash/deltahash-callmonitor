#include "callLogMonitor.h"

telephony_handle_list_s handle_list;

char* _telephony_sim_state_to_string(telephony_sim_state_e sim_state)
{
	switch (sim_state) {
	case TELEPHONY_SIM_STATE_UNAVAILABLE:

		return "TELEPHONY_SIM_STATE_UNAVAILABLE";

	case TELEPHONY_SIM_STATE_LOCKED:
		return "TELEPHONY_SIM_STATE_LOCKED";

	case TELEPHONY_SIM_STATE_AVAILABLE:
		return "TELEPHONY_SIM_STATE_AVAILABLE";

	case TELEPHONY_SIM_STATE_UNKNOWN:
		return "TELEPHONY_SIM_STATE_UNKNOWN";

	default:
		return "Unknown";
	}
}

void sim_state_noti_cb(telephony_h handle, telephony_noti_e noti_id, void* data, void* user_data)
{
	telephony_sim_state_e sim_state = *(int*)data;
	char* sim_state_string = _telephony_sim_state_to_string(sim_state);
	dlog_print(DLOG_DEBUG, LOG_TAG, "SIM state: [%s]", sim_state_string);
}

char* _telephony_network_state_to_string(telephony_network_service_state_e network_state)
{
    switch (network_state) {
    case TELEPHONY_NETWORK_SERVICE_STATE_IN_SERVICE:
        return "TELEPHONY_NETWORK_SERVICE_STATE_IN_SERVICE";

    case TELEPHONY_NETWORK_SERVICE_STATE_OUT_OF_SERVICE:
        return "TELEPHONY_NETWORK_SERVICE_STATE_OUT_OF_SERVICE";

    case TELEPHONY_NETWORK_SERVICE_STATE_EMERGENCY_ONLY:
        return "TELEPHONY_NETWORK_SERVICE_STATE_EMERGENCY_ONLY";

    default:
        return "Unknown";
    }
}

void network_service_state_noti_cb(telephony_h handle, telephony_noti_e noti_id, void* data,
                                   void* user_data)
{
    telephony_network_service_state_e network_state = *(int *)data;
    char* network_state_string = _telephony_network_state_to_string(network_state);
    dlog_print(DLOG_DEBUG, LOG_TAG, "SIM state: [%s]", network_state_string);
}

/* Application scenarios */

void _init_my_telephony_info()
{
	telephony_handle_list_s handle_list;
	int i;
	char* state = NULL;
	telephony_error_e ret;

	/*  Create a telephony handle */
	ret = telephony_init(&handle_list); // In case of single SIM, we get only one handle
	if (ret != TELEPHONY_ERROR_NONE)
		dlog_print(DLOG_ERROR, LOG_TAG, "[telephony_init] failed");
	else {
		for (i = 0; i < handle_list.count; i++) {

			/* Get the state of the SIM card */
			telephony_sim_state_e sim_state;
			ret = telephony_sim_get_state(handle_list.handle[i], &sim_state);
			state = _telephony_sim_state_to_string(sim_state);

			/*
			 * Register the sim_state_noti_cb() callback function
			 * to be notified if the state of the SIM card changes
			 */
			ret = telephony_set_noti_cb(handle_list.handle[i], TELEPHONY_NOTI_SIM_STATUS,
										sim_state_noti_cb, NULL);
			if (ret != TELEPHONY_ERROR_NONE) {
				dlog_print(DLOG_ERROR, LOG_TAG,"[telephony_sim_get_state] failed");
				dlog_print(DLOG_ERROR, LOG_TAG,"[telephony_set_noti_cb] failed");
			}

			if (sim_state == TELEPHONY_SIM_STATE_AVAILABLE) {

				/* Get ICCID information from a SIM card - Integrated Circuit Card IDentification (ICCID) */
				ret = telephony_sim_get_icc_id(handle_list.handle[i], &my_sim_info.iccid[i]);
				if (ret != TELEPHONY_ERROR_NONE)
					dlog_print(DLOG_ERROR, LOG_TAG,"[telephony_sim_get_icc_id] failed");

				/* Get Operator information from a SIM card */
				ret = telephony_sim_get_operator(handle_list.handle[i], &my_sim_info.operator_id[i]);
				if (ret != TELEPHONY_ERROR_NONE)
					dlog_print(DLOG_ERROR, LOG_TAG,"[telephony_sim_get_operator] failed");

				/* Get MSIN information from a SIM card - Mobile Subscription Identification Number(MSIN) */
				ret = telephony_sim_get_msin(handle_list.handle[i], &my_sim_info.msin[i]);
				if (ret != TELEPHONY_ERROR_NONE)
					dlog_print(DLOG_ERROR, LOG_TAG,"[telephony_sim_get_msin] failed");

				/* Get SPN information from a SIM card - Service Provider Name (SPN) */
				ret = telephony_sim_get_spn(handle_list.handle[i], &my_sim_info.spn[i]);
				if (ret != TELEPHONY_ERROR_NONE)
					dlog_print(DLOG_ERROR, LOG_TAG,"[telephony_sim_get_spn] failed");

				/* Get subscriber id information from a SIM card */
				ret = telephony_sim_get_subscriber_id(handle_list.handle[i], &my_sim_info.subscriber_id[i]);
				if (ret != TELEPHONY_ERROR_NONE)
					dlog_print(DLOG_ERROR, LOG_TAG,"[telephony_sim_get_subscriber_id] failed");

				/* Get subscriber number information from a SIM card */
				ret = telephony_sim_get_subscriber_number(handle_list.handle[i], &my_sim_info.subscriber_number[i]);
				if (ret != TELEPHONY_ERROR_NONE)
					dlog_print(DLOG_ERROR, LOG_TAG,"[telephony_sim_get_subscriber_number] failed");

				/* Get the Network Service state */
				telephony_network_service_state_e network_service_state;
				ret = telephony_network_get_service_state(handle_list.handle[i], &network_service_state);
				state = _telephony_network_state_to_string(network_service_state);
				if (ret != TELEPHONY_ERROR_NONE)
					dlog_print(DLOG_ERROR, LOG_TAG, "[telephony_network_get_service_state] failed");

				/*
				* Register the network_service_state_noti_cb() callback function
				* to be notified if the Network Service state changes
				*/
				ret = telephony_set_noti_cb(handle_list.handle[i], TELEPHONY_NOTI_NETWORK_SERVICE_STATE, network_service_state_noti_cb, NULL);
				if (ret != TELEPHONY_ERROR_NONE)
					dlog_print(DLOG_ERROR, LOG_TAG, "[telephony_set_noti_cb] failed");

				/* Get the MNC information - Mobile Network Codes (MNC) */
				ret = telephony_network_get_mnc(handle_list.handle[i], &my_sim_info.mnc[i]);
				if (ret != TELEPHONY_ERROR_NONE)
					dlog_print(DLOG_ERROR, LOG_TAG,"[telephony_network_get_mnc] failed");

				/* Get the modem information - International Mobile Station Equipment Identity (IMEI) */
				ret = telephony_modem_get_imei(handle_list.handle[i], &my_sim_info.imei[i]);
				if (ret != TELEPHONY_ERROR_NONE)
					dlog_print(DLOG_ERROR, LOG_TAG,"[telephony_modem_get_imei] failed");

				/* Get the Cell ID information */
				ret = telephony_network_get_cell_id(handle_list.handle[i], &my_sim_info.cell_id[i]);
				if (ret != TELEPHONY_ERROR_NONE)
					dlog_print(DLOG_ERROR, LOG_TAG,"[telephony_network_get_cell_id] failed");
			}
		}
	}
}

/* Function to print info for test */
void _print_my_telephony_info()
{
	telephony_handle_list_s handle_list;
	int i;
	telephony_error_e ret;

	/*  Create a telephony handle */
	ret = telephony_init(&handle_list); // In case of single SIM, we get only one handle
	if (ret != TELEPHONY_ERROR_NONE)
		dlog_print(DLOG_ERROR, LOG_TAG, "[telephony_init] failed");
	else {
		for (i = 0; i < handle_list.count; i++)
		{
			telephony_sim_state_e sim_state;
			ret = telephony_sim_get_state(handle_list.handle[i], &sim_state);
			dlog_print(DLOG_DEBUG, LOG_TAG, "SIM-%d : Integrated Circuit Card IDentification: %s", i + 1, my_sim_info.iccid[i]);
			dlog_print(DLOG_DEBUG, LOG_TAG, "SIM-%d : Operator: %s", i + 1, my_sim_info.operator_id[i]);
			dlog_print(DLOG_DEBUG, LOG_TAG, "SIM-%d : MSIN: %s", i + 1, my_sim_info.msin[i]);
			dlog_print(DLOG_DEBUG, LOG_TAG, "SIM-%d : SPN: %s", i + 1, my_sim_info.spn[i]);
			dlog_print(DLOG_DEBUG, LOG_TAG, "SIM-%d : Subscriber ID: %s", i + 1, my_sim_info.subscriber_id[i]);
			dlog_print(DLOG_DEBUG, LOG_TAG, "SIM-%d : Subscriber Number: %s", i + 1, my_sim_info.subscriber_number[i]);
			dlog_print(DLOG_DEBUG, LOG_TAG, "SIM-%d : MNC: %s", i + 1, my_sim_info.mnc[i]);
			dlog_print(DLOG_DEBUG, LOG_TAG, "SIM-%d : IMEI: %s", i + 1, my_sim_info.imei[i]);
			dlog_print(DLOG_DEBUG, LOG_TAG, "SIM-%d : Cell ID: %d", i + 1, my_sim_info.cell_id[i]);
			dlog_print(DLOG_DEBUG, LOG_TAG, "==============================================================");
		}
	}
}
