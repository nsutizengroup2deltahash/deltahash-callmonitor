#include "calllogmonitor.h"

QueryData* makeQueryData(int id, int mnc, char* package_name, float cost_per_second) {
	QueryData* query_data;

	query_data = malloc(sizeof(QueryData));
	memset(query_data, 0x0, sizeof(QueryData));

	query_data->id = id;
	query_data->mnc = mnc;
	query_data->package_name = package_name;
	query_data->cost_per_second = cost_per_second;

	return query_data;
}

void populatePackageData() {
	vector_init(&package_data);

	VECTOR_ADD(package_data, makeQueryData(1, 1, "GP-Same-Package1", 1.15));
	VECTOR_ADD(package_data, makeQueryData(2, 1, "GP-Other-Package1", 3.00));
	VECTOR_ADD(package_data, makeQueryData(3, 1, "GP-Same-Package2", 1.10));
	VECTOR_ADD(package_data, makeQueryData(4, 1, "GP-Other-Package2", 2.55));
	VECTOR_ADD(package_data, makeQueryData(5, 1, "GP-Flat-Package3", 2.75));

	VECTOR_ADD(package_data, makeQueryData(6, 2, "RB-Same-Package1", 0.50));
	VECTOR_ADD(package_data, makeQueryData(7, 2, "RB-Other-Package1", 1.80));
	VECTOR_ADD(package_data, makeQueryData(8, 2, "RB-Same-Package2", 1.10));
	VECTOR_ADD(package_data, makeQueryData(9, 2, "RB-Other-Package2", 2.10));
	VECTOR_ADD(package_data, makeQueryData(10, 2, "RB-Flat-Package3", 2.00));

	VECTOR_ADD(package_data, makeQueryData(11, 7, "AT-Same-Package1", 1.10));
	VECTOR_ADD(package_data, makeQueryData(12, 7, "AT-Other-Package1", 1.30));
	VECTOR_ADD(package_data, makeQueryData(13, 7, "AT-Same-Package2", 1.00));
	VECTOR_ADD(package_data, makeQueryData(14, 7, "AT-Other-Package2", 1.15));

	VECTOR_ADD(package_data, makeQueryData(15, 3, "BL-Same-Package1", 1.33));
	VECTOR_ADD(package_data, makeQueryData(16, 3, "BL-Other-Package1", 1.40));
	VECTOR_ADD(package_data, makeQueryData(17, 3, "BL-Same-Package2", 1.00));
	VECTOR_ADD(package_data, makeQueryData(18, 3, "BL-Other-Package2", 1.25));

	dlog_print(DLOG_DEBUG, "package", "size of vector: %d", VECTOR_TOTAL(package_data));

	QueryData query_data_t;

	for (int i = 0; i < VECTOR_TOTAL(package_data); i++) {
		query_data_t = *VECTOR_GET(package_data, QueryData*, i);
		dlog_print(DLOG_DEBUG, "package", "id: %d \nmnc: %d \npackage name: %s \ncost per second: %f \n\n", query_data_t.id, query_data_t.mnc, query_data_t.package_name, query_data_t.cost_per_second);
	}
}

float getCostPerSecondFromPackageData(char* package_name) {
	QueryData query_data_t;
	for (int i = 0; i < VECTOR_TOTAL(package_data); i++) {
		query_data_t = *VECTOR_GET(package_data, QueryData*, i);
		if (strncmp(query_data_t.package_name, package_name, 20) == 0) {
			return query_data_t.cost_per_second;
		}
	}
	return -999999999999;
}


