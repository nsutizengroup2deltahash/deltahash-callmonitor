//#include "sqliteDbHelper.h"
//#include "calllogmonitor.h"
//
//#define BUFLEN 500 /*assume buffer length for query string's size.*/
//
//sqlite3 *sampleDb; /*name of database*/
//int select_row_count = 0;
//
///*open database instance*/
//int opendb()
//{
//     char * dataPath = app_get_shared_data_path(); /*fetched package path available physically in the device*/
//	 int size = strlen(dataPath)+10;
//
//	 char * path = malloc(sizeof(char)*size);
//
//	 strcpy(path,dataPath);
//	 strncat(path, DB_NAME, size);
//
//    dlog_print( DLOG_DEBUG, "DbTest", "DB Path:%s", path);
//	/* DBG("DB Path = [%s]", path); prepared full path, database will be stored there*/
//
//
//	 int ret = sqlite3_open_v2( path , &sampleDb, SQLITE_OPEN_CREATE|SQLITE_OPEN_READWRITE, NULL);
//	 if(ret != SQLITE_OK)
//		/* ERR("DB Create Error! [%s]", sqlite3_errmsg(sampleDb)); */
//		 dlog_print( DLOG_DEBUG, "DbTest", "DB Create Error! [%s]",sqlite3_errmsg(sampleDb));
//
//	 free(dataPath);
//	 free(path);
//         /*didn't close database instance as this will be handled by caller e.g. insert, delete*/
//	 return ret;
//}
//
//int initdb()
//{
//	if (opendb() != SQLITE_OK) /*create database instance*/
//		return SQLITE_ERROR;
//
//   int ret;
//   char *ErrMsg;
//
//   char *sql = "CREATE TABLE IF NOT EXISTS "\
//   		    TABLE_NAME" ("  \
//   		    COL_PACKAGE_NAME" TEXT NOT NULL, " \
//   			COL_MNC" INTEGER NOT NULL, " \
//   			COL_COST_PER_SEC" FLOAT NOT NULL, " \
//   			COL_DATE" TEXT NOT NULL, " \
//   			COL_ID" INTEGER PRIMARY KEY AUTOINCREMENT);"; /*id autoincrement*/
//
////   char* sql = package_information_create_table();
//
//   /*DBG("crate table query : %s", sql);*/
//   dlog_print(DLOG_DEBUG, "DbTest", "create table query : %s",sql);
//   ret = sqlite3_exec(sampleDb, sql, NULL, 0, &ErrMsg); /*execute query*/
//   if(ret != SQLITE_OK)
//   {
//	   dlog_print( DLOG_DEBUG, "DbTest","Table Create Error! [%s]",ErrMsg);
//	   /*ERR("Table Create Error! [%s]", ErrMsg);*/
//	   sqlite3_free(ErrMsg);
//	   sqlite3_close(sampleDb); /*close db instance as instance is still open*/
//
//	   return SQLITE_ERROR;
//   }
//   dlog_print( DLOG_DEBUG, "DbTest","Db Table created successfully!");
//   /*DBG("Db Table created successfully!");*/
//   sqlite3_close(sampleDb); /*close the db instance as operation is done here*/
//
//   return SQLITE_OK;
//}
//
///*callback for insert operation*/
//static int insertcb(void *NotUsed, int argc, char **argv, char **azColName){
//   int i;
//   for(i=0; i<argc; i++){
//      /*usually we do not need to do anything.*/
//   }
//   return 0;
//}
//
//int insertIntoDb(int mnc, const char * package_name, float cost_per_second) {
//
//	if(opendb() != SQLITE_OK) /*create database instance*/
//		return SQLITE_ERROR;
//
//	char sqlbuff[BUFLEN];
//	char *ErrMsg;
//	int ret;
//	/*read system date time using sqlite function*/
//	char* dateTime = "strftime('%Y-%m-%d  %H-%M','now')";
//
//	/*prepare query for INSERT operation*/
//	snprintf(sqlbuff, BUFLEN, "INSERT INTO "\
//			TABLE_NAME" VALUES(\'%s\', %d, %f, %s, NULL);", /*didn't include id as it is autoincrement*/
//						package_name, mnc, cost_per_second, dateTime);
//	/*DBG("Insert query = [%s]", sqlbuff);*/
//	dlog_print( DLOG_DEBUG, "DbTest","Insert query = [%s]", sqlbuff);
//
//	ret = sqlite3_exec(sampleDb, sqlbuff, insertcb, 0, &ErrMsg); /*execute query*/
//	dlog_print(DLOG_DEBUG, "DbTest","sqlite executed successfully... status: %d", ret);
//	dlog_print(DLOG_ERROR, "DbTest","Insert query = %s", ErrMsg);
//
//	if (ret != SQLITE_OK)
//	{
//		dlog_print( DLOG_DEBUG, "DbTest","Insertion Error! [%s]", sqlite3_errmsg(sampleDb));
//		/* ERR("Insertion Error! [%s]", sqlite3_errmsg(sampleDb));*/
//	   sqlite3_free(ErrMsg);
//	   sqlite3_close(sampleDb); /*close db instance for failed case*/
//
//	   return SQLITE_ERROR;
//	}
//
//	sqlite3_close(sampleDb); /*close db instance for success case*/
//
//	return SQLITE_OK;
//}
//
//QueryData *qrydata;
//
///*this callback will be called for each row fetched from database. we need to handle retrieved elements for each row manually and store data for further use*/
//static int selectAllItemcb(void *data, int argc, char **argv, char **azColName){
//        /*
//        * SQLite queries return data in argv parameter as  character pointer */
//        /*prepare a temporary structure*/
//	QueryData *temp = (QueryData*)realloc(qrydata, ((select_row_count + 1) * sizeof(QueryData)));
//
//	if(temp == NULL){
//		/*ERR("Cannot reallocate memory for QueryData");*/
//		dlog_print( DLOG_DEBUG, "DbTest","Cannot reallocate memory for QueryData");
//		return SQLITE_ERROR;
//	} else {
//                /*store data into temp structure*/
//		strcpy(temp[select_row_count].package_name, argv[0]);
//		temp[select_row_count].mnc = atoi(argv[1]);
//		temp[select_row_count].cost_per_second = atoi(argv[2]);
//		strcpy(temp[select_row_count].date, argv[3]);
//		temp[select_row_count].id = atoi(argv[4]);
//
//                /*copy temp structure into main sturct*/
//		qrydata = temp;
//	}
//
//	select_row_count ++; /*keep row count*/
//
//   return SQLITE_OK;
//}
//
//int getAllFromDb(QueryData **query_data, int* num_of_rows) {
//	if(opendb() != SQLITE_OK) /*create database instance*/
//		return SQLITE_ERROR;
//
//	qrydata = (QueryData *) calloc (1, sizeof(QueryData)); /*preparing local querydata struct*/
//
//	char *sql = "SELECT * FROM SampleTable ORDER BY ID DESC"; /*select query*/
//	int ret;
//	char *ErrMsg;
//	select_row_count = 0;
//
//	ret = sqlite3_exec(sampleDb, sql, selectAllItemcb, (void*)query_data, &ErrMsg);
//	if (ret != SQLITE_OK)
//	{
//		dlog_print( DLOG_DEBUG, "DbTest","select query execution error [%s]", ErrMsg);
//	   /*DBG("select query execution error [%s]", ErrMsg);*/
//	   sqlite3_free(ErrMsg);
//	   sqlite3_close(sampleDb); /*close db for failed case*/
//
//	   return SQLITE_ERROR;
//	}
//
//		/*assign all retrived values into caller's pointer*/
//	*query_data = qrydata;
//	*num_of_rows = select_row_count;
//	dlog_print( DLOG_DEBUG, "DbTest","select query execution success!");
//	/* DBG("select query execution success!"); */
//	sqlite3_close(sampleDb); /*close db for success case*/
//
//	return SQLITE_OK;
//}
//
//int getQueryById(QueryData **query_data, int id) {
//	if(opendb() != SQLITE_OK) /*create database instance*/
//		return SQLITE_ERROR;
//
//	qrydata = (QueryData *) calloc (1, sizeof(QueryData));
//
//	char sql[BUFLEN];
//	snprintf(sql, BUFLEN, "SELECT * FROM "\
//			TABLE_NAME" where "\
//			COL_ID"=%d;", id);
//	dlog_print( DLOG_DEBUG, "DbTest5", "sql: %s", sql);
//
//	int ret = 0;
//	char *ErrMsg;
//
//	ret = sqlite3_exec(sampleDb, sql, selectAllItemcb, (void*)query_data, &ErrMsg);
//	if (ret != SQLITE_OK)
//	{
//		dlog_print( DLOG_DEBUG, "DbTest","select query execution error [%s]", ErrMsg);
//	 /*  DBG("select query execution error [%s]", ErrMsg); */
//	   sqlite3_free(ErrMsg);
//	   sqlite3_close(sampleDb);
//
//	   return SQLITE_ERROR;
//	}
//	dlog_print( DLOG_DEBUG, "DbTest","select query execution success!");
//	/* DBG("select query execution success!"); */
//
//		/*assign fetched data into caller's struct*/
//	*query_data = qrydata;
//
//	sqlite3_close(sampleDb); /*close db*/
//
//	return SQLITE_OK;
//}
//
//int getQueryByPackageName(QueryData **query_data, char *package_name) {
//	if(opendb() != SQLITE_OK) /*create database instance*/
//		return SQLITE_ERROR;
//
//	qrydata = (QueryData *) calloc (1, sizeof(QueryData));
//
//	char sql[BUFLEN];
//	snprintf(sql, BUFLEN, "SELECT * FROM "\
//			TABLE_NAME" where "\
//			COL_PACKAGE_NAME" LIKE \"%s\";", package_name);
//	dlog_print(DLOG_DEBUG, "DbTest1","%s", sql);
//
//	int ret = 0;
//	char *ErrMsg;
//
//	ret = sqlite3_exec(sampleDb, sql, selectAllItemcb, (void*)query_data, &ErrMsg);
//	if (ret != SQLITE_OK)
//	{
//		dlog_print( DLOG_DEBUG, "DbTest","select query execution error [%s]", ErrMsg);
//	 /*  DBG("select query execution error [%s]", ErrMsg); */
//	   sqlite3_free(ErrMsg);
//	   sqlite3_close(sampleDb);
//
//	   return SQLITE_ERROR;
//	}
//	dlog_print( DLOG_DEBUG, "DbTest","select query execution success!");
//	/* DBG("select query execution success!"); */
//
//		/*assign fetched data into caller's struct*/
//	*query_data = qrydata;
//
//	sqlite3_close(sampleDb); /*close db*/
//
//	return SQLITE_OK;
//}
//
//static int deletecb(void *data, int argc, char **argv, char **azColName){
//   int i;
//   for(i=0; i<argc; i++){
//	/*no need to do anything*/
//   }
//
//   return 0;
//}
//
//int deleteQueryById(int id) {
//	if(opendb() != SQLITE_OK) /*create database instance*/
//		return SQLITE_ERROR;
//
//	char sql[BUFLEN];
//	snprintf(sql, BUFLEN, "DELETE from SampleTable where ID=%d;", id);
//
//	int counter = 0, ret = 0;
//	char *ErrMsg;
//
//	ret = sqlite3_exec(sampleDb, sql, deletecb, &counter, &ErrMsg);
//	if (ret != SQLITE_OK)
//	{
//		dlog_print( DLOG_DEBUG, "DbTest", "Delete Error! [%s]", sqlite3_errmsg(sampleDb));
//		/*ERR("Delete Error! [%s]", sqlite3_errmsg(sampleDb));*/
//		sqlite3_free(ErrMsg);
//		sqlite3_close(sampleDb);
//
//		return SQLITE_ERROR;
//	}
//
//	sqlite3_close(sampleDb);
//
//	return SQLITE_OK;
//}
//
//int deleteAll() {
//
//	if(opendb() != SQLITE_OK) /*create database instance*/
//		return SQLITE_ERROR;
//
//	char sql[BUFLEN];
//	snprintf(sql, BUFLEN, "DELETE from SampleTable;");
//
//	int counter = 0, ret = 0;
//	char *ErrMsg;
//
//	ret = sqlite3_exec(sampleDb, sql, deletecb, &counter, &ErrMsg);
//	if (ret != SQLITE_OK)
//	{
//		dlog_print( DLOG_DEBUG, "DbTest","Delete Error! [%s]", sqlite3_errmsg(sampleDb));
//		/* ERR("Delete Error! [%s]", sqlite3_errmsg(sampleDb)); */
//		sqlite3_free(ErrMsg);
//		sqlite3_close(sampleDb);
//
//		return SQLITE_ERROR;
//	}
//
//	sqlite3_close(sampleDb);
//
//	return SQLITE_OK;
//}
//
//int g_row_count = 0;
//
//static int row_count_cb(void *data, int argc, char **argv, char **azColName)
//{
//	g_row_count = atoi(argv[0]); /*number of rows*/
//
//	return 0;
//}
//
//int getTotalItemsCount(int* num_of_rows) {
//	if(opendb() != SQLITE_OK) /*create database instance*/
//		return SQLITE_ERROR;
//
//	char *sql = "SELECT COUNT(*) FROM SampleTable;";
//	char *ErrMsg;
//
//	int ret = 0;
//
//	ret = sqlite3_exec(sampleDb, sql, row_count_cb, NULL, &ErrMsg);
//	if (ret != SQLITE_OK)
//	{
//	/*	ERR("Count Error! [%s]", sqlite3_errmsg(sampleDb)); */
//		dlog_print( DLOG_DEBUG, "DbTest","Count Error! [%s]", sqlite3_errmsg(sampleDb));
//		sqlite3_free(ErrMsg);
//		sqlite3_close(sampleDb);
//
//		return SQLITE_ERROR;
//	}
//
//	/* DBG("Total row found[%d]", g_row_count); */
//	dlog_print( DLOG_DEBUG, "DbTest","Total row found[%d]", g_row_count);
////	sqlite3_close(sampleDb);
//
//	*num_of_rows = g_row_count;
//	g_row_count = 0;
//	return SQLITE_OK;
//}
//
//void insertQuery(int mnc, char *package_name, float cost_per_second) {
//	int ret = 0;
//	QueryData* msgdata;
//
//	/*allocate msgdata memory. this will be used for retrieving data from database*/
//	msgdata = (QueryData*) calloc (1, sizeof(QueryData));
//	char buf[MAX_LEN];
//
//	sprintf(buf, "%d",2);
//	strcpy(msgdata->package_name, buf);
//
//	sprintf(buf, "%d", 12);
//	strcpy(msgdata->date, buf);
//
//	ret = insertIntoDb(mnc, package_name, cost_per_second);
//
//	dlog_print(DLOG_DEBUG, "DbTest15", "Called insertIntoDb function...Status: %d", ret);
//}
//
//void showQuery() {
//	int ret;
//	int num_of_rows = 0;
//
//	QueryData* msgdata;
//
//	/*allocate msgdata memory. this will be used for retrieving data from database*/
//	msgdata = (QueryData*) calloc (1, sizeof(QueryData));
//
//	ret = getAllFromDb(&msgdata, &num_of_rows);
//
//	dlog_print(DLOG_DEBUG, "dbtest5", "Called getAllMsgdromDb function...Status: %d", ret);
//	dlog_print(DLOG_DEBUG, "dbtest5", "Returned number of rows: %d", num_of_rows);
//
//	// num_of_rows is incremented by extra 1 by the callback function selectAllItemcb
//	num_of_rows--;
//	while(num_of_rows > -1) {
//		dlog_print(DLOG_DEBUG, "dbtest10", "id: %d, mnc: %d, package_name: %s, cost_per_second: %d, date: %s ",
//				msgdata[num_of_rows].id, msgdata[num_of_rows].mnc, msgdata[num_of_rows].package_name,
//				msgdata[num_of_rows].cost_per_second, msgdata[num_of_rows].date);
//
//		num_of_rows--;
//	}
//}
//
//float getCostPerSecondFromPackageName(char* package_name) {
//
//	QueryData* query_data;
//	int ret = 0;
//	float cost_per_second = 0;
//
//	ret = getQueryByPackageName(&query_data, package_name);
//
//	dlog_print(DLOG_DEBUG, "DbTest", "Called getCostFromPackageName function... status: %d", ret);
//
//	cost_per_second = query_data->cost_per_second;
//
//	return cost_per_second;
//}
//
//void database_init() {
////	opendb();
////	int ret = initdb();
////	dlog_print(DLOG_DEBUG, "DbTest", "Called initdb function...Status: %d", ret);
//
//	insertQuery(1, "GP-Same-Package1", 2.00);
//	insertQuery(1, "GP-Other-Package1", 2.20);
//	insertQuery(1, "GP-Same-Package2", 1.10);
//	insertQuery(1, "GP-Other-Package2", 2.75);
//	insertQuery(1, "GP-Flat-Package3", 2.75);
//
//	insertQuery(2, "RB-Same-Package1", 0.5);
//	insertQuery(2, "RB-Other-Package1", 1.80);
//	insertQuery(2, "RB-Same-Package2", 1.10);
//	insertQuery(2, "RB-Other-Package2", 2.10);
//	insertQuery(2, "RB-Flat-Package3", 2.00);
//
//	insertQuery(7, "AT-Same-Package1", 1.10);
//	insertQuery(7, "AT-Other-Package1", 1.30);
//	insertQuery(7, "AT-Same-Package2", 1.00);
//	insertQuery(7, "AT-Other-Package2", 1.15);
//
//	insertQuery(3, "BL-Same-Package1", 1.33);
//	insertQuery(3, "BL-Other-Package1", 1.40);
//	insertQuery(3, "BL-Same-Package2", 1.00);
//	insertQuery(3, "BL-Other-Package2", 1.25);
//
////	showQuery();
//
//}
//
//
