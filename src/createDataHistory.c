#include "callLogMonitor.h"

#define GRAMEENPHONE "BGD-GP"
#define AIRTEL "airtel"
#define ROBI "Robi"
#define BANGLALINK "Banglalink"
#define ERROR "Error"

call_information current_call_information;

void set_current_call_number (char* call_number) {
	current_call_information.call_number = call_number;
	dlog_print(DLOG_DEBUG, LOG_TAG, "Current Call Number: %s", current_call_information.call_number);
}

void set_current_call_duration (int call_duration) {
	current_call_information.call_duration = call_duration;
	dlog_print(DLOG_DEBUG, LOG_TAG, "Current Call Duration: %d", current_call_information.call_duration);
}

void set_current_sim_id(int sim_id) {
	current_call_information.sim_id = sim_id;
	dlog_print(DLOG_DEBUG, LOG_TAG, "Current Sim Identification: %d", current_call_information.sim_id);
}

char* getOperatorId(char *num)
{
	dlog_print(DLOG_DEBUG, "number", "size: %d", strlen(num));
	dlog_print(DLOG_DEBUG, "number", "size: %c", num[3]);


	if (strlen(num) == 11) {

		switch(num[2]) {
			case '6':
				return AIRTEL;
			case '7':
				return GRAMEENPHONE;
			case '8':
				return ROBI;
			case '9':
				return BANGLALINK;
			default:
				return ERROR;
		}
	} else if (strlen(num) == 14) {

		switch(num[5]) {
			case '6':
				return AIRTEL;
			case '7':
				return GRAMEENPHONE;
			case '8':
				return ROBI;
			case '9':
				return BANGLALINK;
			default:
				return ERROR;
		}
	}

	return ERROR;
}

void _populate_my_history_data(char* call_number, int call_duration, int sim_id)
{
	set_current_call_number(call_number);
	set_current_call_duration(call_duration);
	set_current_sim_id(sim_id);

//	int operatorId = getOperatorId(call_number);


	if (sim_id == 0)
	{
		//TODO: Implement Logic
	}
	else if (sim_id == 1)
	{
		//TODO: Implement Logic
	}
}

void print_call_information() {
	dlog_print(DLOG_DEBUG, "CLM7", "vector size %d", VECTOR_TOTAL(call_log_information));

	call_record call_record_data_t;

	for (int i = 0; i < VECTOR_TOTAL(call_log_information); i++) {
		call_record_data_t = *VECTOR_GET(call_log_information, call_record*, i);

		dlog_print(DLOG_DEBUG, "CLM7", "#log_no: %d", i+1);
		dlog_print(DLOG_DEBUG, "CLM7", "#log_type: %d", call_record_data_t.log_type);
		dlog_print(DLOG_DEBUG, "CLM7", "#log_time: %d", call_record_data_t.log_time);
		dlog_print(DLOG_DEBUG, "CLM7", "#call_number: %s", call_record_data_t.call_number);
		dlog_print(DLOG_DEBUG, "CLM7", "#call_duration: %d", call_record_data_t.call_duration);
		dlog_print(DLOG_DEBUG, "CLM7", "#sim_slot_no: %d", call_record_data_t.sim_slot_no);
	}

}

