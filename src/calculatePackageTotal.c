#include "calllogmonitor.h"

void calculateTotalDuration(char* spn)
{
	total_duration_same = 0;
	total_duration_other = 0;
	for (int i = 0; i < VECTOR_TOTAL(call_log_information); i++) {
		call_record call_log_data_t = *VECTOR_GET(call_log_information, call_record*, i);

		dlog_print(DLOG_DEBUG, "operator", "Caller ID: %s", call_log_data_t.call_number);
		dlog_print(DLOG_DEBUG, "operator", "Operator ID: %s", getOperatorId(call_log_data_t.call_number));

		if (strncmp(spn, getOperatorId(call_log_data_t.call_number), 10) == 0) {
			total_duration_same += call_log_data_t.call_duration;
		} else {
			total_duration_other += call_log_data_t.call_duration;
		}
	}
}

void calculateTotalCostForGrameenPackage() {
	float total_cost_same, total_cost_other;

	total_cost_same = total_duration_same * getCostPerSecondFromPackageData("GP-Same-Package1");
	total_cost_other = total_duration_other * getCostPerSecondFromPackageData("GP-Other-Package1");
	total_cost_package1 = total_cost_same + total_cost_other;
	package1 = "GP-Nishchinto";
	dlog_print(DLOG_DEBUG, "totalcost", "%f %f %f", total_cost_same, total_cost_other, total_cost_package1);

	total_cost_same = total_duration_same * getCostPerSecondFromPackageData("GP-Same-Package2");
	total_cost_other = total_duration_other * getCostPerSecondFromPackageData("GP-Other-Package2");
	total_cost_package2 = total_cost_same + total_cost_other;
	package2 = "GP-Bondhu";
	dlog_print(DLOG_DEBUG, "totalcost", "%f %f %f", total_cost_same, total_cost_other, total_cost_package2);

	total_cost_same = total_duration_same * getCostPerSecondFromPackageData("GP-Flat-Package3");
	total_cost_other = total_duration_other * getCostPerSecondFromPackageData("GP-Flat-Package3");
	total_cost_package3 = total_cost_same + total_cost_other;
	package3 = "GP-Djuice";
	dlog_print(DLOG_DEBUG, "totalcost", "%f %f %f", total_cost_same, total_cost_other, total_cost_package3);
}

void calculateTotalCostForAirtelPackage() {
	float total_cost_same, total_cost_other;

	total_cost_same = total_duration_same * getCostPerSecondFromPackageData("AT-Same-Package1");
	total_cost_other = total_duration_other * getCostPerSecondFromPackageData("AT-Other-Package1");
	package1 = "AT-Shobai Ek";
	total_cost_package1 = total_cost_same + total_cost_other;

	dlog_print(DLOG_DEBUG, "totalcost", "%f %f %f", total_cost_same, total_cost_other, total_cost_package1);

	total_cost_same = total_duration_same * getCostPerSecondFromPackageData("AT-Same-Package2");
	total_cost_other = total_duration_other * getCostPerSecondFromPackageData("AT-Other-Package2");
	total_cost_package2 = total_cost_same + total_cost_other;
	package2 = "AT-Ajibon 1 Paisa";
	dlog_print(DLOG_DEBUG, "totalcost", "%f %f %f", total_cost_same, total_cost_other, total_cost_package2);

	total_cost_same = total_duration_same * 999999999999999999;
	total_cost_other = total_duration_other * 999999999999999999;
	total_cost_package3 = total_cost_same + total_cost_other;
	package3 = "No Such Package";
	dlog_print(DLOG_DEBUG, "totalcost", "%f %f %f", total_cost_same, total_cost_other, total_cost_package3);
}

void calculateTotalCostForBanglalinkPackage() {
	float total_cost_same, total_cost_other;

	total_cost_same = total_duration_same * getCostPerSecondFromPackageData("BL-Same-Package1");
	total_cost_other = total_duration_other * getCostPerSecondFromPackageData("BL-Other-Package1");
	total_cost_package1 = total_cost_same + total_cost_other;
	package1 = "BL-Desh Hello";
	dlog_print(DLOG_DEBUG, "totalcost", "%f %f %f", total_cost_same, total_cost_other, total_cost_package1);

	total_cost_same = total_duration_same * getCostPerSecondFromPackageData("BL-Same-Package2");
	total_cost_other = total_duration_other * getCostPerSecondFromPackageData("BL-Other-Package2");
	total_cost_package2 = total_cost_same + total_cost_other;
	package2 = "BL-Desh Ek";
	dlog_print(DLOG_DEBUG, "totalcost", "%f %f %f", total_cost_same, total_cost_other, total_cost_package2);

	total_cost_same = total_duration_same * 999999999999999999;
	total_cost_other = total_duration_other * 999999999999999999;
	total_cost_package3 = total_cost_same + total_cost_other;
	package3 = "No Such Package";
	dlog_print(DLOG_DEBUG, "totalcost", "%f %f %f", total_cost_same, total_cost_other, total_cost_package3);
}

void calculateTotalCostForRobiPackage() {
	float total_cost_same, total_cost_other;

	total_cost_same = total_duration_same * getCostPerSecondFromPackageData("RB-Same-Package1");
	total_cost_other = total_duration_other * getCostPerSecondFromPackageData("RB-Other-Package1");
	total_cost_package1 = total_cost_same + total_cost_other;
	package1 = "RB-Goti 36";
	dlog_print(DLOG_DEBUG, "totalcost", "%f %f %f", total_cost_same, total_cost_other, total_cost_package1);

	total_cost_same = total_duration_same * getCostPerSecondFromPackageData("RB-Same-Package2");
	total_cost_other = total_duration_other * getCostPerSecondFromPackageData("RB-Other-Package2");
	total_cost_package2 = total_cost_same + total_cost_other;
	package2 = "RB-Nobanno 37";
	dlog_print(DLOG_DEBUG, "totalcost", "%f %f %f", total_cost_same, total_cost_other, total_cost_package2);

	total_cost_same = total_duration_same * getCostPerSecondFromPackageData("RB-Flat-Package3");
	total_cost_other = total_duration_other * getCostPerSecondFromPackageData("RB-Flat-Package3");
	total_cost_package3 = total_cost_same + total_cost_other;
	package3 = "RB-Hoot Hut Chomok 32";
	dlog_print(DLOG_DEBUG, "totalcost", "%f %f %f", total_cost_same, total_cost_other, total_cost_package3);
}

void calculateCostBasedonSpn(char* spn) {
	calculateTotalDuration(spn);
	if (strncmp(spn, "BDP-GP", 10)) {
		calculateTotalCostForGrameenPackage();
	} else if (strncmp(spn, "airtel", 10)) {
		calculateTotalCostForAirtelPackage();
	} else if (strncmp(spn, "Banglalink", 10)) {
		calculateTotalCostForBanglalinkPackage();
	} else if (strncmp(spn, "Robi", 10)) {
		calculateTotalCostForRobiPackage();
	}
}

float min(float a, float b) {
	return a <= b ? a : b;
}

char* getPackageWithMinimumCost() {
	float minimum_cost = min(total_cost_package1, min(total_cost_package2, total_cost_package3));
	dlog_print(DLOG_DEBUG, "minimum", "%f %f %f %f", total_cost_package1, total_cost_package2, total_cost_package3, minimum_cost);

	if (minimum_cost == total_cost_package1) {
		return package1;
	} else if (minimum_cost == total_cost_package2) {
		return package2;
	} else if (minimum_cost == total_cost_package3) {
		return package3;
	}

	return "Error";
}































