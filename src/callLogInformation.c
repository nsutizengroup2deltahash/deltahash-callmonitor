#include "callLogMonitor.h"

int init_ok = -1;

static void contacts_init()
{
    init_ok = contacts_connect();

    if (init_ok != CONTACTS_ERROR_NONE) {
        dlog_print(DLOG_ERROR, LOG_TAG, "contacts_connect() failed: %d", init_ok);
    }
}

static void _free_call_record_data(call_record *call_record_data)
{
    if (NULL == call_record_data)
        return;

    free(call_record_data->call_number);
    free(call_record_data);
}

static call_record* _create_call_record_data(contacts_record_h record)
{
	call_record *call_record_data;

    call_record_data = malloc(sizeof(call_record));
    memset(call_record_data, 0x0, sizeof(call_record));

    if (CONTACTS_ERROR_NONE != contacts_record_get_int(record, _contacts_phone_log.log_type, &call_record_data->log_type)) {
        dlog_print(DLOG_ERROR, LOG_TAG, "get log type failed ");
        _free_call_record_data(call_record_data);

        return NULL;
    }
    if (CONTACTS_ERROR_NONE != contacts_record_get_str(record, _contacts_phone_log.address, &call_record_data->call_number)) {
        dlog_print(DLOG_ERROR, LOG_TAG, "get called number failed ");
        _free_call_record_data(call_record_data);

        return NULL;
    }
    if (CONTACTS_ERROR_NONE != contacts_record_get_int(record, _contacts_phone_log.log_time, &call_record_data->log_time)) {
        dlog_print(DLOG_ERROR, LOG_TAG, "get log time failed ");
        _free_call_record_data(call_record_data);

        return NULL;
    }
    if (CONTACTS_ERROR_NONE != contacts_record_get_int(record, _contacts_phone_log.extra_data1, &call_record_data->call_duration)) {
        dlog_print(DLOG_ERROR, LOG_TAG, "get call duration failed ");
        _free_call_record_data(call_record_data);

       return NULL;
    }
    if (CONTACTS_ERROR_NONE != contacts_record_get_int(record, _contacts_phone_log.sim_slot_no, &call_record_data->sim_slot_no)) {
        dlog_print(DLOG_ERROR, LOG_TAG, "get sim slot no failed ");
        _free_call_record_data(call_record_data);

       return NULL;
    }

    return call_record_data;
}

void get_phone_logs()
{
    int error_code;
    contacts_list_h list = NULL;
    contacts_query_h query = NULL;
    contacts_filter_h filter = NULL;
    contacts_record_h record;
    call_record *call_record_data;

    vector_init(&call_log_information);

    error_code = contacts_query_create(_contacts_phone_log._uri , &query);
    if (error_code != CONTACTS_ERROR_NONE) {
        dlog_print(DLOG_ERROR, LOG_TAG, "contacts_query_create() failed: %d", error_code);
    }

    error_code = contacts_filter_create(_contacts_phone_log._uri, &filter);
    if (error_code != CONTACTS_ERROR_NONE) {
        dlog_print(DLOG_ERROR, LOG_TAG, "contacts_filter_create() failed: %d", error_code);
    }

    error_code = contacts_filter_add_int(filter, _contacts_phone_log.log_type, CONTACTS_MATCH_EQUAL, CONTACTS_PLOG_TYPE_VOICE_OUTGOING);
    if (error_code != CONTACTS_ERROR_NONE) {
        dlog_print(DLOG_ERROR, LOG_TAG, "contacts_filter_add_int() failed: %d", error_code);
    }


    error_code = contacts_query_set_filter(query, filter);
    if (error_code != CONTACTS_ERROR_NONE) {
        dlog_print(DLOG_ERROR, LOG_TAG, "contacts_query_set_filter() failed: %d", error_code);
    }

    error_code = contacts_db_get_records_with_query(query, 0, 0, &list);
    if (error_code != CONTACTS_ERROR_NONE) {
        dlog_print(DLOG_ERROR, LOG_TAG, "contacts_db_get_records_with_query() failed: %d",
                   error_code);
    }

    contacts_filter_destroy(filter);
    contacts_query_destroy(query);

    while (contacts_list_get_current_record_p(list, &record) == CONTACTS_ERROR_NONE) {
        call_record_data = _create_call_record_data(record);

        VECTOR_ADD(call_log_information, call_record_data);

        error_code = contacts_list_next(list);
        if (error_code != CONTACTS_ERROR_NONE)
            break;
    }


    contacts_list_destroy(list, true);
}

void contacts_deinit(void *data)
{
    int error_code;
    error_code = contacts_disconnect();
    if (error_code != CONTACTS_ERROR_NONE)
        dlog_print(DLOG_ERROR, LOG_TAG, "contacts_disconnect() failed: %d", error_code);
}

void call_information_init() {
	contacts_init();
	get_phone_logs();
}

void call_information_deinit(void *data) {
	contacts_deinit(data);
}
