#include "callLogMonitor.h"
#include "sqliteDbHelper.h"

static void
win_delete_request_cb(void* data, Evas_Object* obj, void* event_info)
{
	ui_app_exit();
}

static void
win_back_cb(void* data, Evas_Object* obj, void* event_info)
{
	appdata_s* ad = data;
	/* Let window go to hide state. */
	elm_win_lower(ad->win);
}

static void
hoversel1_clicked_cb(void* data EINA_UNUSED, Evas_Object* obj EINA_UNUSED, void* event_info EINA_UNUSED)
{
	dlog_print(DLOG_INFO, LOG_TAG, "Hover button 1 is clicked.\n");
}

static void
hoversel2_clicked_cb(void* data EINA_UNUSED, Evas_Object* obj EINA_UNUSED, void* event_info EINA_UNUSED)
{
	dlog_print(DLOG_INFO, LOG_TAG, "Hover button 2 is clicked.\n");
}

static void
hoversel1_selected_cb(void* data EINA_UNUSED, Evas_Object* obj EINA_UNUSED, void* event_info)
{
	const char* txt = elm_object_item_text_get(event_info);

	dlog_print(DLOG_INFO, LOG_TAG, "Hover button 1 is selected. (Selected : %s)\n", txt);
}

static void
hoversel2_selected_cb(void* data EINA_UNUSED, Evas_Object* obj EINA_UNUSED, void* event_info)
{
	const char* txt = elm_object_item_text_get(event_info);

	dlog_print(DLOG_INFO, LOG_TAG, "Hover button 2 is selected. (Selected : %s)\n", txt);
}

static void
hoversel1_dismissed_cb(void* data EINA_UNUSED, Evas_Object* obj EINA_UNUSED, void* event_info EINA_UNUSED)
{
	dlog_print(DLOG_INFO, LOG_TAG, "Hover button 1 is dismissed.\n");
}

static void
hoversel2_dismissed_cb(void* data EINA_UNUSED, Evas_Object* obj EINA_UNUSED, void* event_info EINA_UNUSED)
{
	dlog_print(DLOG_INFO, LOG_TAG, "Hover button 2 is dismissed.\n");
}

static void
btn_clicked_cb(void* data, Evas_Object* obj, void* event_info)
{
	char buf1[500];
	char buf2[500];

	if (my_sim_info.spn[0] != NULL) {
		snprintf(buf1, sizeof buf1, "<color=#00008BFF><align=center> %s <align></color>", selected_package_sim1);
		dlog_print(DLOG_DEBUG, "buffer", "%s", buf1);

		elm_object_text_set(label1, _(buf1));
		evas_object_show(label1);
	}

	if (my_sim_info.spn[1] != NULL) {
		snprintf(buf2, sizeof buf2, "<color=#00008BFF><align=center> %s <align></color>", selected_package_sim2);
		dlog_print(DLOG_DEBUG, "buffer", "%s", buf2);

		elm_object_text_set(label2, _(buf2));
		evas_object_show(label2);
	}
}

static void
create_base_gui(appdata_s* ad)
{

	_init_my_telephony_info();
	call_information_init();
	populatePackageData();

	char* sim1_package_info[] = { "Package 01", "Package 02", "Package 03" };
	char* sim2_package_info[] = { "Package 01", "Package 02", "Package 03" };

	/* Window */
	/* Create and initialize elm_win.
	   elm_win is mandatory to manipulate window. */
	ad->win = elm_win_util_standard_add(PACKAGE, PACKAGE);
	elm_win_autodel_set(ad->win, EINA_TRUE);

	if (elm_win_wm_rotation_supported_get(ad->win)) {
		int rots[4] = { 0, 90, 180, 270 };
		elm_win_wm_rotation_available_rotations_set(ad->win, (const int*)(&rots), 4);
	}

	evas_object_smart_callback_add(ad->win, "delete,request", win_delete_request_cb, NULL);
	eext_object_event_callback_add(ad->win, EEXT_CALLBACK_BACK, win_back_cb, ad);

	/* Conformant */
	/* Create and initialize elm_conformant.
	   elm_conformant is mandatory for base gui to have proper size
	   when indicator or virtual keypad is visible. */
	ad->conform = elm_conformant_add(ad->win);
	elm_win_indicator_mode_set(ad->win, ELM_WIN_INDICATOR_SHOW);
	elm_win_indicator_opacity_set(ad->win, ELM_WIN_INDICATOR_OPAQUE);
	evas_object_size_hint_weight_set(ad->conform, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	elm_win_resize_object_add(ad->win, ad->conform);
	evas_object_show(ad->conform);

	/* Label */
	/* Create an actual view of the base gui.
	   Modify this part to change the view. */
	/*ad->label = elm_label_add(ad->conform);
	elm_object_text_set(ad->label, "<align=center>Hello Tizen</align>");
	evas_object_size_hint_weight_set(ad->label, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	elm_object_content_set(ad->conform, ad->label);*/

	/* Add box for base layout */
	Evas_Object* box = elm_box_add(ad->conform);
	evas_object_size_hint_align_set(box, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_size_hint_weight_set(box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	elm_object_content_set(ad->conform, box);

	if(my_sim_info.spn[0] == NULL && my_sim_info.spn[1] == NULL )
	{
		/* View no contents page */
		Evas_Object* nocontents = elm_layout_add(box);
		elm_layout_theme_set(nocontents, "layout", "nocontents", "default");
		evas_object_size_hint_weight_set(nocontents, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
		evas_object_size_hint_align_set(nocontents, EVAS_HINT_FILL, EVAS_HINT_FILL);
		elm_object_part_text_set(nocontents, "elm.text", "No SIM Inserted");
		elm_object_part_text_set(nocontents, "elm.help.text", "Please insert a SIM card.");
		elm_layout_signal_emit(nocontents, "align.center", "elm");

		evas_object_show(nocontents);
		elm_box_pack_end(box, nocontents);
	}
	else
	{
		if(my_sim_info.spn[0] != NULL)
		{
			/* Get Package Data For Sim 1*/
			calculateCostBasedonSpn(my_sim_info.spn[0]);
			selected_package_sim1 = getPackageWithMinimumCost();
			dlog_print(DLOG_DEBUG, "packagedata", "Best Package Sim 1: %s", selected_package_sim1);

			/* Add Hoversel 1 expanding to screen width */
			Evas_Object* hoversel1 = elm_hoversel_add(box);
			elm_hoversel_hover_parent_set(hoversel1, ad->conform);
			elm_object_text_set(hoversel1, my_sim_info.spn[0]);

			for (int i = 0; i < 3; i++) {
				elm_hoversel_item_add(hoversel1, sim1_package_info[i], NULL, ELM_ICON_NONE, NULL, NULL);
			}

			evas_object_size_hint_align_set(hoversel1, EVAS_HINT_FILL, 0.5);
			evas_object_size_hint_weight_set(hoversel1, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
			evas_object_show(hoversel1);
			elm_box_pack_end(box, hoversel1);

			evas_object_smart_callback_add(hoversel1, "clicked", hoversel1_clicked_cb, NULL);
			evas_object_smart_callback_add(hoversel1, "selected", hoversel1_selected_cb, NULL);
			evas_object_smart_callback_add(hoversel1, "dismissed", hoversel1_dismissed_cb, NULL);

			/* Add Label 1 to show text */
			label1 = elm_label_add(box);
			elm_object_part_content_set(box, "label1", label1);
//			elm_object_text_set(label1, _("<color=#00008BFF><align=center> - - text - - <align></color>"));
			elm_object_disabled_set(label1, EINA_TRUE);
//			evas_object_show(label1);
			elm_box_pack_end(box, label1);
		}

		if(my_sim_info.spn[1] != NULL)
		{
			/* Get Package Data for Sim 2 */
			calculateCostBasedonSpn(my_sim_info.spn[1]);
			selected_package_sim2 = getPackageWithMinimumCost();
			dlog_print(DLOG_DEBUG, "packagedata", "Best Package Sim 2: %s", selected_package_sim2);

			/* Add Hoversel 2 expanding to screen width */
			Evas_Object* hoversel2 = elm_hoversel_add(box);
			elm_hoversel_hover_parent_set(hoversel2, ad->conform);
			elm_object_text_set(hoversel2, my_sim_info.spn[1]);

			for (int j = 0; j < 3; j++) {
				elm_hoversel_item_add(hoversel2, sim2_package_info[j], NULL, ELM_ICON_NONE, NULL, NULL);
			}

			evas_object_size_hint_align_set(hoversel2, EVAS_HINT_FILL, 0.5);
			evas_object_size_hint_weight_set(hoversel2, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
			evas_object_show(hoversel2);
			elm_box_pack_end(box, hoversel2);

			evas_object_smart_callback_add(hoversel2, "clicked", hoversel2_clicked_cb, NULL);
			evas_object_smart_callback_add(hoversel2, "selected", hoversel2_selected_cb, NULL);
			evas_object_smart_callback_add(hoversel2, "dismissed", hoversel2_dismissed_cb, NULL);

			/* Label 2 to show text */
			label2 = elm_label_add(box);
			elm_object_part_content_set(box, "label1", label2);
//			elm_object_text_set(label2, _("<color=#00008BFF><align=center> - - text - - <align></color>"));
			elm_object_disabled_set(label2, EINA_TRUE);
//			evas_object_show(label2);
			elm_box_pack_end(box, label2);
		}

		/* button 1 */
		Evas_Object* btn = elm_button_add(box);
		elm_object_style_set(btn, "bottom");
		elm_object_text_set(btn, "OK");

		if(my_sim_info.spn[0] == NULL && my_sim_info.spn[1] == NULL)
		{
			elm_object_disabled_set(btn, EINA_TRUE);
		}

		evas_object_smart_callback_add(btn, "clicked", btn_clicked_cb, NULL);
		evas_object_size_hint_weight_set(btn, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
		evas_object_size_hint_align_set(btn, EVAS_HINT_FILL, 0.5);
		evas_object_show(btn);
		elm_box_pack_end(box, btn);
	}

	/* Show window after base gui is set up */
	evas_object_show(ad->win);

	//print_call_information();
}

static bool
app_create(void* data)
{
	/* Hook to take necessary actions before main event loop starts
		Initialize UI resources and application's data
		If this function returns true, the main loop of application starts
		If this function returns false, the application is terminated */
	appdata_s* ad = data;

	create_base_gui(ad);

	return true;
}

static void
app_control(app_control_h app_control, void* data)
{
	/* Handle the launch request. */
}

static void
app_pause(void* data)
{
	/* Take necessary actions when application becomes invisible. */
}

static void
app_resume(void* data)
{
	/* Take necessary actions when application becomes visible. */
}

static void
app_terminate(void* data)
{
	call_information_deinit(data);
	/* Release all resources. */
}

static void
ui_app_lang_changed(app_event_info_h event_info, void* user_data)
{
	/*APP_EVENT_LANGUAGE_CHANGED*/
	char* locale = NULL;
	system_settings_get_value_string(SYSTEM_SETTINGS_KEY_LOCALE_LANGUAGE, &locale);
	elm_language_set(locale);
	free(locale);
	return;
}

static void
ui_app_orient_changed(app_event_info_h event_info, void* user_data)
{
	/*APP_EVENT_DEVICE_ORIENTATION_CHANGED*/
	return;
}

static void
ui_app_region_changed(app_event_info_h event_info, void* user_data)
{
	/*APP_EVENT_REGION_FORMAT_CHANGED*/
}

static void
ui_app_low_battery(app_event_info_h event_info, void* user_data)
{
	/*APP_EVENT_LOW_BATTERY*/
}

static void
ui_app_low_memory(app_event_info_h event_info, void* user_data)
{
	/*APP_EVENT_LOW_MEMORY*/
}

int
main(int argc, char* argv[])
{
	appdata_s ad = {0,};
	int ret = 0;

	ui_app_lifecycle_callback_s event_callback = {0,};
	app_event_handler_h handlers[5] = {NULL, };

	event_callback.create = app_create;
	event_callback.terminate = app_terminate;
	event_callback.pause = app_pause;
	event_callback.resume = app_resume;
	event_callback.app_control = app_control;

	ui_app_add_event_handler(&handlers[APP_EVENT_LOW_BATTERY], APP_EVENT_LOW_BATTERY, ui_app_low_battery, &ad);
	ui_app_add_event_handler(&handlers[APP_EVENT_LOW_MEMORY], APP_EVENT_LOW_MEMORY, ui_app_low_memory, &ad);
	ui_app_add_event_handler(&handlers[APP_EVENT_DEVICE_ORIENTATION_CHANGED], APP_EVENT_DEVICE_ORIENTATION_CHANGED, ui_app_orient_changed, &ad);
	ui_app_add_event_handler(&handlers[APP_EVENT_LANGUAGE_CHANGED], APP_EVENT_LANGUAGE_CHANGED, ui_app_lang_changed, &ad);
	ui_app_add_event_handler(&handlers[APP_EVENT_REGION_FORMAT_CHANGED], APP_EVENT_REGION_FORMAT_CHANGED, ui_app_region_changed, &ad);

	ret = ui_app_main(argc, argv, &event_callback, &ad);
	if (ret != APP_ERROR_NONE) {
		dlog_print(DLOG_ERROR, LOG_TAG, "app_main() is failed. err = %d", ret);
	}

	return ret;
}
